# Startup for VacS-TGTV:SC-IOC-001

# Load required modules
require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load PLC specific startup script
iocshLoad("$(IOCSH_TOP)/iocsh/tgt_pbwrv_vac_plc_001.iocsh", "DBDIR=$(IOCSH_TOP)/db/, MODVERSION=$(IOCVERSION=)")

